clear all; close all; clc;
disp(' ');
disp('===================================================================')
disp('[GenModel] Generate io model ...');

% Load IO information
disp('[GenModel] Load io definition ...');
input_def = '_InportDefinition.csv';
output_def = '_OutportDefinition.csv';

inTable = readtable(input_def);
outTable = readtable(output_def);

% Create bus object
disp('[GenModel] Create bus object ...');
eval([inTable.Properties.VariableNames{2},';']);
eval([outTable.Properties.VariableNames{2},';']);

% Open new simulink system
model_name = ['IO_model','_',datestr(now,'yymmdd_HHMMSS')];
model_h = new_system(model_name);
open_system(model_h);
disp(['[GenModel] Open simulink model - ',model_name]);

% Create input structure
disp('[GenModel] Create inport ...');
default_pos_diff = 75;
port_pos_diff = 3000;
sub_pos_diff = 300;
select_pos_diff = 400;
sat_pos_diff = 200;
from_pos_diff = 400;
sub_port_pos_diff = 1200;

from_gap = 15;
from_hor_size = 205;
from_ver_size = 30;

idx_Left    = 1;
idx_Top     = 2;
idx_Right   = 3;
idx_Bottom  = 4;

for i_idx=1:length(inTable.IN)
    %% Create inport
    bus_type_name = inTable{i_idx,1}{1};
    inport_name = inTable{i_idx,2}{1};
    inport_name = replace(inport_name,'(','_');
    inport_name = replace(inport_name,')','');
    inport_name = replace(inport_name,' ','_');
    inport_h = add_block('simulink/Sources/In1',[model_name,'/',inport_name]);
    vec_inport_pos = get_param(inport_h,'Position');
    vec_inport_pos([idx_Top idx_Bottom]) = ...
        vec_inport_pos([idx_Top idx_Bottom])+default_pos_diff*(i_idx-1);
    set_param(inport_h,'Position',vec_inport_pos);
    bus_def_str = ['Bus: ',bus_type_name];
    set_param(inport_h,'OutDataTypeStr',bus_def_str);
    disp(['    Inport(',num2str(i_idx),') - ',inport_name]);

    %% Create input processing subsystem
    sub_name = ['IP_',inport_name];
    sub_h = add_block('simulink/Ports & Subsystems/Subsystem',[model_name,'/',sub_name]);
    vec_sub_pos = get_param(sub_h,'Position');
    vec_inport_center = get_center_point(vec_inport_pos);
    vec_sub_center = get_center_point(vec_sub_pos);
    vec_sub_diff_center = vec_sub_center-vec_inport_center;
    vec_sub_pos([idx_Left idx_Right]) = ...
        vec_sub_pos([idx_Left idx_Right])+sub_pos_diff-vec_sub_diff_center(1);
    vec_sub_pos([idx_Top idx_Bottom]) = ...
        vec_sub_pos([idx_Top idx_Bottom])-vec_sub_diff_center(2);
    set_param(sub_h,'Position',vec_sub_pos);

    % Connect line between inport & subsystem
    inport_ph = get_param(inport_h,'PortHandles');
    sub_ph = get_param(sub_h,'PortHandles');
    line_h = add_line(model_name,inport_ph.Outport(1),sub_ph.Inport(1),'autorouting','on');
    set_param(line_h,'Name',inport_name);

    %% Set inport & output of subsystem
    sub_inport_name = find_system([model_name,'/',sub_name],'SearchDepth',1,'BlockType','Inport');
    sub_outport_name = find_system([model_name,'/',sub_name],'SearchDepth',1,'BlockType','Outport');
    sub_inport_name = sub_inport_name{1};
    sub_outport_name = sub_outport_name{1};
    sub_inport_h = get_param(sub_inport_name,'handle');
    sub_outport_h = get_param(sub_outport_name,'handle');
    set_param(sub_inport_h,'Name',['IN_',inport_name]);
    set_param(sub_outport_h,'Name',['OUT_',inport_name]);
    set_param(sub_inport_h,'OutDataTypeStr',bus_def_str);
    set_param(sub_outport_h,'OutDataTypeStr',bus_def_str);
    vec_sub_inport_pos = get_param(sub_inport_h,'Position');
    vec_sub_outport_pos = get_param(sub_outport_h,'Position');
    vec_sub_inport_center = get_center_point(vec_sub_inport_pos);
    vec_sub_outport_center = get_center_point(vec_sub_outport_pos);
    vec_outport_diff_center = vec_sub_outport_center-vec_sub_inport_center;
    vec_sub_outport_pos([idx_Left idx_Right]) = ...
        vec_sub_outport_pos([idx_Left idx_Right])+sub_port_pos_diff-vec_outport_diff_center(1);
    set_param(sub_outport_h,'Position',vec_sub_outport_pos);

    %% Create subsystem bus selector
    select_h = add_block('simulink/Signal Routing/Bus Selector',[model_name,'/',sub_name,'/Bus Selector']);
    vec_select_pos = get_param(select_h,'Position');
    vec_select_center = get_center_point(vec_select_pos);
    vec_select_diff_center = vec_select_center-vec_sub_inport_center;
    vec_select_pos([idx_Left idx_Right]) = ...
        vec_select_pos([idx_Left idx_Right])+select_pos_diff-vec_select_diff_center(1);
    vec_select_pos([idx_Top idx_Bottom]) = ...
        vec_select_pos([idx_Top idx_Bottom])-vec_select_diff_center(2);
    set_param(select_h,'Position',vec_select_pos);

    % Connect line between inport & bus selector
    sub_inport_ph = get_param(sub_inport_h,'PortHandles');
    sub_outport_ph = get_param(sub_outport_h,'PortHandles');
    select_ph = get_param(select_h,'PortHandles');
    delete_line([model_name,'/',sub_name],sub_inport_ph.Outport(1),sub_outport_ph.Inport(1));
    sel_line_h = add_line([model_name,'/',sub_name], ...
        sub_inport_ph.Outport(1),select_ph.Inport(1),'autorouting','on');
    set_param(sel_line_h,'Name',inport_name);

    % Expand bus selector
    element_list = get_param(select_h,'InputSignals');
    output_str = '';
    for j_idx=1:length(element_list)
        output_str = [output_str,element_list{j_idx},','];
    end
    output_str = output_str(1:end-1);
    set_param(select_h,'OutputSignals',output_str);
    vec_select_pos = get_param(select_h,'Position');
    vec_select_center = get_center_point(vec_select_pos);
    vertical_size = default_pos_diff*length(element_list);
    vec_select_pos(idx_Top) = vec_select_center(2)-vertical_size/2;
    vec_select_pos(idx_Bottom) = vec_select_center(2)+vertical_size/2;
    set_param(select_h,'Position',vec_select_pos);

    %% Create subsystem bus creator
    bus_type = eval(bus_type_name);
    create_h = add_block('simulink/Signal Routing/Bus Creator',[model_name,'/',sub_name,'/Bus Creator']);
    vec_create_pos = get_param(create_h,'Position');
    vec_sub_outport_center = get_center_point(vec_sub_outport_pos);
    vec_create_center = get_center_point(vec_create_pos);
    vec_create_diff_center = vec_create_center-vec_sub_outport_center;
    vec_create_pos([idx_Left idx_Right]) = ...
        vec_create_pos([idx_Left idx_Right])-select_pos_diff-vec_create_diff_center(1);
    vec_create_pos([idx_Top idx_Bottom]) = ...
        vec_create_pos([idx_Top idx_Bottom])-vec_create_diff_center(2);
    set_param(create_h,'Position',vec_create_pos);
    set_param(create_h,'OutDataTypeStr',bus_def_str);
    set_param(create_h,'Inputs',num2str(length(bus_type.Elements)));

    % Connect line between bus creator & outport
    create_ph = get_param(create_h,'PortHandles');
    create_line_h = add_line([model_name,'/',sub_name],...
        create_ph.Outport(1),sub_outport_ph.Inport(1),'autorouting','on');
    set_param(create_line_h,'Name',inport_name);

    % Expand bus creator
    vec_create_pos = get_param(create_h,'Position');
    vec_create_center = get_center_point(vec_create_pos);
    vertical_size = default_pos_diff*length(bus_type.Elements);
    vec_create_pos(idx_Top) = vec_create_center(2)-vertical_size/2;
    vec_create_pos(idx_Bottom) = vec_create_center(2)+vertical_size/2;
    set_param(create_h,'Position',vec_create_pos);

    %% Create type saturation
    total_vertical_size = default_pos_diff*(length(bus_type.Elements)-1);
    for j_idx=1:length(bus_type.Elements)
        % Get min & max value
        data_type = bus_type.Elements(j_idx).DataType;
        [sat_min_str, sat_max_str] = get_min_max(data_type);
        if (strcmp('boolean',data_type))
            % Connect line between selector & creator
            select_ph = get_param(select_h,'PortHandles');
            bool_line_h = add_line([model_name,'/',sub_name], ...
                select_ph.Outport(j_idx),create_ph.Inport(j_idx),'autorouting','on');
        else
            % Add saturation
            sat_h = add_block('simulink/Discontinuities/Saturation', ...
                [model_name,'/',sub_name,'/Saturation',num2str(j_idx)]);
            set_param(sat_h,'LowerLimit',sat_min_str);
            set_param(sat_h,'UpperLimit',sat_max_str);
            if (strcmp(sat_min_str,'-inf') && strcmp(sat_max_str,'inf'))
                % Infinity case
                set_param(sat_h,'BackgroundColor','red');
            else
                % Normal case
                set_param(sat_h,'BackgroundColor','cyan');
            end
            annot_str = ['Auto',newline,'[ %<LowerLimit> : %<UpperLimit> ]'];
            set_param(sat_h,'AttributesFormatString',annot_str);
            vertical_offset = -total_vertical_size/2+default_pos_diff*(j_idx-1);
            vec_sat_pos = get_param(sat_h,'Position');
            vec_sat_center = get_center_point(vec_sat_pos);
            vec_select_center = get_center_point(vec_select_pos);
            vec_sat_diff_center = vec_sat_center-vec_select_center;
            vec_sat_pos([idx_Left idx_Right]) = ...
                vec_sat_pos([idx_Left idx_Right])+sat_pos_diff-vec_sat_diff_center(1);
            vec_sat_pos([idx_Top idx_Bottom]) = ...
                vec_sat_pos([idx_Top idx_Bottom])+vertical_offset-vec_sat_diff_center(2);
            set_param(sat_h,'Position',vec_sat_pos);

            % Connect line between selector & saturation
            select_ph = get_param(select_h,'PortHandles');
            sat_ph = get_param(sat_h,'PortHandles');
            sat_line_h = add_line([model_name,'/',sub_name], ...
                select_ph.Outport(j_idx),sat_ph.Inport(1),'autorouting','on');

            % Connect line between saturation & creator
            sat_line_h = add_line([model_name,'/',sub_name], ...
                sat_ph.Outport(1),create_ph.Inport(j_idx),'autorouting','on');
            sat_line_name = bus_type.Elements(j_idx).Name;
            set_param(sat_line_h,'Name',sat_line_name);
        end
        
    end

    %% Create subsystem goto
    goto_h = add_block('simulink/Signal Routing/Goto',[model_name,'/Goto',num2str(i_idx)]);
    set_param(goto_h,'GotoTag',inport_name);
    vec_goto_pos = get_param(goto_h,'Position');
    vec_goto_center = get_center_point(vec_goto_pos);
    vec_sub_center = get_center_point(vec_sub_pos);
    vec_goto_diff_center = vec_goto_center-vec_sub_center;
    vec_goto_pos(idx_Left) = vec_goto_center(1)-from_hor_size/2-vec_goto_diff_center(1)+from_pos_diff;
    vec_goto_pos(idx_Right) = vec_goto_center(1)+from_hor_size/2-vec_goto_diff_center(1)+from_pos_diff;
    vec_goto_pos(idx_Top) = vec_goto_center(2)-from_ver_size/2-vec_goto_diff_center(2);
    vec_goto_pos(idx_Bottom) = vec_goto_center(2)+from_ver_size/2-vec_goto_diff_center(2);
    set_param(goto_h,'Position',vec_goto_pos);

    % Connect line between subsystem & goto
    goto_ph = get_param(goto_h,'PortHandles');
    goto_line_h = add_line(model_name, ...
        sub_ph.Outport(1),goto_ph.Inport(1),'autorouting','on');
    set_param(goto_line_h,'Name',inport_name);

    %% Create subsystem from
    from_h = add_block('simulink/Signal Routing/From',[model_name,'/From',num2str(i_idx)]);
    set_param(from_h,'GotoTag',inport_name);
    vec_from_pos = get_param(from_h,'Position');
    vec_from_center = get_center_point(vec_from_pos);
    vec_goto_center = get_center_point(vec_goto_pos);
    vec_from_diff_center = vec_from_center-vec_goto_center;
    vec_from_pos(idx_Left) = vec_from_center(1)-from_hor_size/2-vec_from_diff_center(1)+from_hor_size+from_gap;
    vec_from_pos(idx_Right) = vec_from_center(1)+from_hor_size/2-vec_from_diff_center(1)+from_hor_size+from_gap;
    vec_from_pos(idx_Top) = vec_from_center(2)-from_ver_size/2-vec_from_diff_center(2);
    vec_from_pos(idx_Bottom) = vec_from_center(2)+from_ver_size/2-vec_from_diff_center(2);
    set_param(from_h,'Position',vec_from_pos);

    %% Create inport termination
    ter_h = add_block('simulink/Sinks/Terminator',[model_name,'/Terminator',num2str(i_idx)]);
    vec_ter_pos = get_param(ter_h,'Position');
    vec_ter_center = get_center_point(vec_ter_pos);
    vec_from_center = get_center_point(vec_from_pos);
    vec_ter_diff_center = vec_ter_center-vec_from_center;
    vec_ter_pos([idx_Left idx_Right]) = ...
        vec_ter_pos([idx_Left idx_Right])+from_pos_diff-vec_ter_diff_center(1);
    vec_ter_pos([idx_Top idx_Bottom]) = ...
        vec_ter_pos([idx_Top idx_Bottom])-vec_ter_diff_center(2);
    set_param(ter_h,'Position',vec_ter_pos);

    % Connect line between from & terminator
    from_ph = get_param(from_h,'PortHandles');
    ter_ph = get_param(ter_h,'PortHandles');
    from_line_h = add_line(model_name, ...
        from_ph.Outport(1),ter_ph.Inport(1),'autorouting','on');
    set_param(from_line_h,'Name',inport_name);

end


for i_idx=1:length(outTable.OUT)
    %% Create outport
    bus_type_name = outTable{i_idx,1}{1};
    outport_name = outTable{i_idx,2}{1};
    outport_name = replace(outport_name,'(','_');
    outport_name = replace(outport_name,')','');
    outport_name = replace(outport_name,' ','_');
    outport_h = add_block('simulink/Sinks/Out1',[model_name,'/',outport_name]);
    vec_outport_pos = get_param(outport_h,'Position');
    vec_outport_pos([idx_Left idx_Right]) = ...
        vec_outport_pos([idx_Left idx_Right])+port_pos_diff;
    vec_outport_pos([idx_Top idx_Bottom]) = ...
        vec_outport_pos([idx_Top idx_Bottom])+default_pos_diff*(i_idx-1);
    set_param(outport_h,'Position',vec_outport_pos);
    bus_def_str = ['Bus: ',bus_type_name];
    set_param(outport_h,'OutDataTypeStr',bus_def_str);
    disp(['    Outport(',num2str(i_idx),') - ',outport_name]);

    %% Create output processing subsystem
    sub_name = ['OP_',outport_name];
    sub_h = add_block('simulink/Ports & Subsystems/Subsystem',[model_name,'/',sub_name]);
    vec_sub_pos = get_param(sub_h,'Position');
    vec_outport_center = get_center_point(vec_outport_pos);
    vec_sub_center = get_center_point(vec_sub_pos);
    vec_sub_diff_center = vec_sub_center-vec_outport_center;
    vec_sub_pos([idx_Left idx_Right]) = ...
        vec_sub_pos([idx_Left idx_Right])-sub_pos_diff-vec_sub_diff_center(1);
    vec_sub_pos([idx_Top idx_Bottom]) = ...
        vec_sub_pos([idx_Top idx_Bottom])-vec_sub_diff_center(2);
    set_param(sub_h,'Position',vec_sub_pos);

    % Connect line between inport & subsystem
    outport_ph = get_param(outport_h,'PortHandles');
    sub_ph = get_param(sub_h,'PortHandles');
    line_h = add_line(model_name,sub_ph.Outport(1),outport_ph.Inport(1),'autorouting','on');
    set_param(line_h,'Name',outport_name);

    %% Create subsystem ground input
    gnd_h = add_block('simulink/Sources/Ground',[model_name,'/Ground',num2str(i_idx)]);
    vec_gnd_pos = get_param(gnd_h,'Position');
    vec_gnd_center = get_center_point(vec_gnd_pos);
    vec_sub_center = get_center_point(vec_sub_pos);
    vec_gnd_diff_center = vec_gnd_center-vec_sub_center;
    vec_gnd_pos([idx_Left idx_Right]) = ...
        vec_gnd_pos([idx_Left idx_Right])-sub_pos_diff-vec_gnd_diff_center(1);
    vec_gnd_pos([idx_Top idx_Bottom]) = ...
        vec_gnd_pos([idx_Top idx_Bottom])-vec_gnd_diff_center(2);
    set_param(gnd_h,'Position',vec_gnd_pos);

    % Connect line between ground & subsystem
    gnd_ph = get_param(gnd_h,'PortHandles');
    gnd_line_h = add_line(model_name, ...
        gnd_ph.Outport(1),sub_ph.Inport(1),'autorouting','on');

    %% Set inport & output of subsystem
    sub_inport_name = find_system([model_name,'/',sub_name],'SearchDepth',1,'BlockType','Inport');
    sub_outport_name = find_system([model_name,'/',sub_name],'SearchDepth',1,'BlockType','Outport');
    sub_inport_name = sub_inport_name{1};
    sub_outport_name = sub_outport_name{1};
    sub_inport_h = get_param(sub_inport_name,'handle');
    sub_outport_h = get_param(sub_outport_name,'handle');
    set_param(sub_outport_h,'Name',['OUT_',inport_name]);
    set_param(sub_outport_h,'OutDataTypeStr',bus_def_str);
    vec_sub_inport_pos = get_param(sub_inport_h,'Position');
    vec_sub_outport_pos = get_param(sub_outport_h,'Position');
    vec_sub_inport_center = get_center_point(vec_sub_inport_pos);
    vec_sub_outport_center = get_center_point(vec_sub_outport_pos);
    vec_outport_diff_center = vec_sub_outport_center-vec_sub_inport_center;
    vec_sub_outport_pos([idx_Left idx_Right]) = ...
        vec_sub_outport_pos([idx_Left idx_Right])+sub_port_pos_diff*2-vec_outport_diff_center(1);
    set_param(sub_outport_h,'Position',vec_sub_outport_pos);

    % Disconnect subsystem inport & outport
    sub_inport_ph = get_param(sub_inport_h,'PortHandles');
    sub_outport_ph = get_param(sub_outport_h,'PortHandles');
    delete_line([model_name,'/',sub_name],sub_inport_ph.Outport(1),sub_outport_ph.Inport(1));

    %% Create subsystem inport termination
    ter_h = add_block('simulink/Sinks/Terminator',[model_name,'/',sub_name,'/Terminator']);
    vec_ter_pos = get_param(ter_h,'Position');
    vec_ter_center = get_center_point(vec_ter_pos);
    vec_sub_inport_center = get_center_point(vec_sub_inport_pos);
    vec_ter_diff_center = vec_ter_center-vec_sub_inport_center;
    vec_ter_pos([idx_Left idx_Right]) = ...
        vec_ter_pos([idx_Left idx_Right])+from_pos_diff-vec_ter_diff_center(1);
    vec_ter_pos([idx_Top idx_Bottom]) = ...
        vec_ter_pos([idx_Top idx_Bottom])-vec_ter_diff_center(2);
    set_param(ter_h,'Position',vec_ter_pos);

    % Connect line between inport & terminator
    sub_inport_ph = get_param(sub_inport_h,'PortHandles');
    ter_ph = get_param(ter_h,'PortHandles');
    sub_inport_line_h = add_line([model_name,'/',sub_name], ...
        sub_inport_ph.Outport(1),ter_ph.Inport(1),'autorouting','on');

    %% Create subsystem bus creator
    bus_type = eval(bus_type_name);
    create_h = add_block('simulink/Signal Routing/Bus Creator',[model_name,'/',sub_name,'/Bus Creator']);
    vec_create_pos = get_param(create_h,'Position');
    vec_sub_outport_center = get_center_point(vec_sub_outport_pos);
    vec_create_center = get_center_point(vec_create_pos);
    vec_create_diff_center = vec_create_center-vec_sub_outport_center;
    vec_create_pos([idx_Left idx_Right]) = ...
        vec_create_pos([idx_Left idx_Right])-select_pos_diff-vec_create_diff_center(1);
    vec_create_pos([idx_Top idx_Bottom]) = ...
        vec_create_pos([idx_Top idx_Bottom])-vec_create_diff_center(2);
    set_param(create_h,'Position',vec_create_pos);
    set_param(create_h,'OutDataTypeStr',bus_def_str);
    set_param(create_h,'Inputs',num2str(length(bus_type.Elements)));

    % Connect line between bus creator & outport
    create_ph = get_param(create_h,'PortHandles');
    create_line_h = add_line([model_name,'/',sub_name],...
        create_ph.Outport(1),sub_outport_ph.Inport(1),'autorouting','on');
    set_param(create_line_h,'Name',inport_name);

    % Expand bus creator
    vec_create_pos = get_param(create_h,'Position');
    vec_create_center = get_center_point(vec_create_pos);
    vertical_size = default_pos_diff*length(bus_type.Elements);
    vec_create_pos(idx_Top) = vec_create_center(2)-vertical_size/2;
    vec_create_pos(idx_Bottom) = vec_create_center(2)+vertical_size/2;
    set_param(create_h,'Position',vec_create_pos);

    %% Create type saturation
    total_vertical_size = default_pos_diff*(length(bus_type.Elements)-1);
    for j_idx=1:length(bus_type.Elements)
        % Get min & max value
        data_type = bus_type.Elements(j_idx).DataType;
        [sat_min_str, sat_max_str] = get_min_max(data_type);

        % Add type conversion
        conv_h = add_block('simulink/Signal Attributes/Data Type Conversion', ...
            [model_name,'/',sub_name,'/Data Type Conversion',num2str(j_idx)]);
        set_param(conv_h,'OutDataTypeStr',convert_type(data_type));
        vertical_offset = -total_vertical_size/2+default_pos_diff*(j_idx-1);
        vec_conv_pos = get_param(conv_h,'Position');
        vec_conv_center = get_center_point(vec_conv_pos);
        vec_create_center = get_center_point(vec_create_pos);
        vec_conv_diff_center = vec_conv_center-vec_create_center;
        vec_conv_pos([idx_Left idx_Right]) = ...
            vec_conv_pos([idx_Left idx_Right])-sat_pos_diff-vec_conv_diff_center(1);
        vec_conv_pos([idx_Top idx_Bottom]) = ...
            vec_conv_pos([idx_Top idx_Bottom])+vertical_offset-vec_conv_diff_center(2);
        set_param(conv_h,'Position',vec_conv_pos);

        % Connect line between type conversion & bus creator
        conv_ph = get_param(conv_h,'PortHandles');
        conv_line_h = add_line([model_name,'/',sub_name], ...
            conv_ph.Outport(1),create_ph.Inport(j_idx),'autorouting','on');
        conv_line_name = bus_type.Elements(j_idx).Name;
        set_param(conv_line_h,'Name',conv_line_name);

        if (strcmp('boolean',data_type))
            % Set dummy saturation
            vec_sat_pos = get_param(conv_h,'Position');
            sat_ph = conv_ph;
            vec_sat_pos([idx_Left idx_Right]) = ...
                vec_sat_pos([idx_Left idx_Right])-default_pos_diff;
        else
            % Add saturation
            sat_h = add_block('simulink/Discontinuities/Saturation', ...
                [model_name,'/',sub_name,'/Saturation',num2str(j_idx)]);
            set_param(sat_h,'LowerLimit',sat_min_str);
            set_param(sat_h,'UpperLimit',sat_max_str);
            if (strcmp(sat_min_str,'-inf') && strcmp(sat_max_str,'inf'))
                % Infinity case
                set_param(sat_h,'BackgroundColor','red');
            else
                % Normal case
                set_param(sat_h,'BackgroundColor','cyan');
            end
            annot_str = ['Auto',newline,'[ %<LowerLimit> : %<UpperLimit> ]'];
            set_param(sat_h,'AttributesFormatString',annot_str);
            vertical_offset = -total_vertical_size/2+default_pos_diff*(j_idx-1);
            vec_sat_pos = get_param(sat_h,'Position');
            vec_sat_center = get_center_point(vec_sat_pos);
            vec_conv_center = get_center_point(vec_conv_pos);
            vec_sat_diff_center = vec_sat_center-vec_conv_center;
            vec_sat_pos([idx_Left idx_Right]) = ...
                vec_sat_pos([idx_Left idx_Right])-default_pos_diff-vec_sat_diff_center(1);
            vec_sat_pos([idx_Top idx_Bottom]) = ...
                vec_sat_pos([idx_Top idx_Bottom])-vec_sat_diff_center(2);
            set_param(sat_h,'Position',vec_sat_pos);

            % Connect line between saturation & type conversion
            sat_ph = get_param(sat_h,'PortHandles');
            sat_line_h = add_line([model_name,'/',sub_name], ...
                sat_ph.Outport(1),conv_ph.Inport(1),'autorouting','on');
        end

        %% Create subsystem from
        from_h = add_block('simulink/Signal Routing/From',[model_name,'/',sub_name,'/From',num2str(j_idx)]);
        set_param(from_h,'GotoTag',conv_line_name);
        vec_from_pos = get_param(from_h,'Position');
        vec_from_center = get_center_point(vec_from_pos);
        vec_sat_center = get_center_point(vec_sat_pos);
        vec_from_diff_center = vec_from_center-vec_sat_center;
        vec_from_pos(idx_Left) = vec_from_center(1)-from_hor_size/2-vec_from_diff_center(1)-from_pos_diff;
        vec_from_pos(idx_Right) = vec_from_center(1)+from_hor_size/2-vec_from_diff_center(1)-from_pos_diff;
        vec_from_pos(idx_Top) = vec_from_center(2)-from_ver_size/2-vec_from_diff_center(2);
        vec_from_pos(idx_Bottom) = vec_from_center(2)+from_ver_size/2-vec_from_diff_center(2);
        set_param(from_h,'Position',vec_from_pos);

        % Connect line between from & saturation
        from_ph = get_param(from_h,'PortHandles');
        from_line_h = add_line([model_name,'/',sub_name], ...
            from_ph.Outport(1),sat_ph.Inport(1),'autorouting','on');
        set_param(from_line_h,'Name',conv_line_name);

        %% Create subsystem goto
        goto_h = add_block('simulink/Signal Routing/Goto',[model_name,'/',sub_name,'/Goto',num2str(j_idx)]);
        set_param(goto_h,'GotoTag',conv_line_name);
        vec_goto_pos = get_param(goto_h,'Position');
        vec_goto_center = get_center_point(vec_goto_pos);
        vec_from_center = get_center_point(vec_from_pos);
        vec_goto_diff_center = vec_goto_center-vec_from_center;
        vec_goto_pos(idx_Left) = vec_goto_center(1)-from_hor_size/2-vec_goto_diff_center(1)-from_hor_size-from_gap;
        vec_goto_pos(idx_Right) = vec_goto_center(1)+from_hor_size/2-vec_goto_diff_center(1)-from_hor_size-from_gap;
        vec_goto_pos(idx_Top) = vec_goto_center(2)-from_ver_size/2-vec_goto_diff_center(2);
        vec_goto_pos(idx_Bottom) = vec_goto_center(2)+from_ver_size/2-vec_goto_diff_center(2);
        set_param(goto_h,'Position',vec_goto_pos);

        %% Create constant source
        const_h = add_block('simulink/Sources/Constant',[model_name,'/',sub_name,'/Constant',num2str(j_idx)]);
        set_param(const_h,'Value','0');
        vec_const_pos = get_param(const_h,'Position');
        vec_const_center = get_center_point(vec_const_pos);
        vec_goto_center = get_center_point(vec_goto_pos);
        vec_const_diff_center = vec_const_center-vec_goto_center;
        vec_const_pos([idx_Left idx_Right]) = ...
            vec_const_pos([idx_Left idx_Right])-from_pos_diff-vec_const_diff_center(1);
        vec_const_pos([idx_Top idx_Bottom]) = ...
            vec_const_pos([idx_Top idx_Bottom])-vec_const_diff_center(2);
        set_param(const_h,'Position',vec_const_pos);

        % Connect line between constant & goto
        const_ph = get_param(const_h,'PortHandles');
        goto_ph = get_param(goto_h,'PortHandles');
        const_line_h = add_line([model_name,'/',sub_name], ...
            const_ph.Outport(1),goto_ph.Inport(1),'autorouting','on');
        set_param(const_line_h,'Name',conv_line_name);

    end
end
% Save generated io model
save_system(model_name,[model_name,'.mdl']);


%% FUNCTION - get_center_point
function vec_center = get_center_point(vec_position)
idx_Left    = 1;
idx_Top     = 2;
idx_Right   = 3;
idx_Bottom  = 4;
vec_center = [mean(vec_position([idx_Left idx_Right])) ...
    mean(vec_position([idx_Top idx_Bottom]))];

end % END OF FUNCTION


%% FUNCTION - get_center_point
function [min_str, max_str] = get_min_max(data_type_str)
min_str = '';
max_str = '';
switch data_type_str
    case 'boolean'
        min_str = 'false';
        max_str = 'true';
    case {'uint8', 'uint16', 'uint32', 'int8', 'int16', 'int32'}
        min_str = num2str(intmin(data_type_str));
        max_str = num2str(intmax(data_type_str));
    case 'float'
        min_str = num2str(-realmax('single'));
        max_str = num2str(+realmax('single'));
    case 'double'
        min_str = num2str(-realmax(data_type_str));
        max_str = num2str(+realmax(data_type_str));
    otherwise
        min_str = '-inf';
        max_str = 'inf';
end

end % END OF FUNCTION


%% FUNCTION - convert_type
function mat_type_str = convert_type(data_type_str)
mat_type_str = '';
switch data_type_str
    case {'boolean', 'uint8', 'uint16', 'uint32', 'int8', 'int16', 'int32', 'double'}
        mat_type_str = data_type_str;
    case 'float'
        mat_type_str = 'single';
    otherwise
        mat_type_str = 'double';
end

end % END OF FUNCTION

