# Import Module
import pandas as pd
import time

# Import Custom Module
from Module.Common.customLog import loggerSet
import Module.Common.constants as CONST
import Module.bus_template as BUSTEMP


# Define Logger
log = loggerSet.setLog(__name__)


# Main Function
if __name__ == "__main__":
    # Get file input
    fileName: str = 'Data/PLANA_ESICD_For_Subscale_v101.xlsx'
    # sheetName: str = '6. OFP to CLAW INPUT'
    sheetName: str = '7. CLAW to OFP OUTPUT'

    # Set function name
    timeStr: str = time.strftime('%y%m%d_%H%M%S', time.localtime(time.time()))
    titleName: str = sheetName.replace('.', '_').replace(' ', '_')
    functName: str = 'make_bus_' + titleName + '_' + timeStr

    # Read .xlsx
    log.debug("Read file")
    dataPd = pd.read_excel(fileName, sheetName, skiprows=[0])
    log.debug(dataPd)

    # Set bus obect string
    busList: str = ''
    for i_idx in range(0, dataPd.shape[0]):
        # Set bus name
        if (pd.isna(dataPd.loc[i_idx]['Bus Type']) is False):
            # Close previous bus definition
            if (i_idx > 0):
                busList = busList + BUSTEMP.busStr_03
            # Add new bus definition header
            busType: str = dataPd.loc[i_idx]['Bus Type']
            log.debug(busType)
            busList = busList + BUSTEMP.busStr_01 + busType + BUSTEMP.busStr_02

        # Set signal definition string
        sigName: str = dataPd.loc[i_idx]['Variable Name']
        sigName = sigName.replace('(', '_').replace(')', '').replace(' ', '_')
        sigType: str = dataPd.loc[i_idx]['Type']
        sigStr = ('      {\''
                  + f'{sigName}'
                  + '\', [1 1], \''
                  + f'{sigType}'
                  + '\', \'real\', \'Sample\', \'Fixed\', '
                  + '[], [], \'\', \'\'}; ... \n')
        log.debug(sigStr)
        busList = busList + sigStr
    busList = busList + BUSTEMP.busStr_03

    # Set make bus string
    fileList: str = (BUSTEMP.fileStr_01 + functName + BUSTEMP.fileStr_02
                     + busList + BUSTEMP.fileStr_03)

    # Write file
    log.debug(fileList)
    fout = open(f'{functName}.m', 'w')
    fout.write(fileList)
    fout.close()
