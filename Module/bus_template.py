# Define script template string
fileStr_01 = \
  """function cellInfo = """

fileStr_02 = \
  """(varargin)
% bus_k_param returns a cell array containing bus object information
%
% Optional Input: 'false' will suppress a call to Simulink.Bus.cellToObject
%                 when the MATLAB file is executed.
% The order of bus element attributes is as follows:
%   ElementName
%   Dimensions
%   DataType
%   SampleTime
%   Complexity
%   SamplingMode
%   DimensionsMode

suppressObject = false;
if nargin == 1 && islogical(varargin{1}) && varargin{1} == false
    suppressObject = true;
elseif nargin > 1
%     error('Invalid input argument(s) encountered');
    k_input = varargin{2};
end

cellInfo = { ...
"""

fileStr_03 = \
  """
}';

if ~suppressObject

    % Create bus objects in the MATLAB base workspace
    Simulink.Bus.cellToObject(cellInfo)
end
end


"""

# Define bus template string
busStr_01 = \
  """
  { ...
    '"""

busStr_02 = \
  """', ...
    '', ...
    '', ...
    'Auto', ...
    '-1', ...
    '0', {...
"""
busStr_03 = \
  """    } ...
  } ...
"""
